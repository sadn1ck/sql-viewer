import { defineStore } from 'pinia'
import type { IAllConnections, IConnection } from '@/types'

export const useNewConnectionStore = defineStore('newconnection', {
  state: (): IConnection => ({
    uid: '',
    name: '',
    hostname: '',
    password: '',
    port: '',
    connected: false,
  }),
  actions: {
  },
})

export const useAllConnectionsStore = defineStore('connections', {
  state: (): IAllConnections => {
    return {
      connections: [],
      activeConnection: '',
    }
  },
  actions: {
    setCurrentConnection(id: string) {
      this.activeConnection = id
    },
  },
})

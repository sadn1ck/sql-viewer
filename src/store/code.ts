import { defineStore } from 'pinia'
import type { Tabs } from '@/types'

export const useEditorPanes = defineStore('editorpanes', {
  state: (): Tabs => {
    return {
      panes: [
        { title: 'OK Example', code: 'SELECT * FROM table_name;', uid: '1' },
        { title: 'Error Example', code: 'INSERT FROM table_name into;', uid: '2' },
        { title: 'Tab 3', code: 'Neutral', uid: '3' }],
    }
  },
})

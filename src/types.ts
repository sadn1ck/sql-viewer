interface IConnection {
  uid: string
  name: string
  hostname: string
  password: string
  port: string
  connected: boolean
}

interface IAllConnections {
  connections: IConnection[]
  activeConnection: string
}

interface EditorPane {
  uid: string
  code: string
  title: string
}

interface Tabs {
  panes: EditorPane[]
}

export type { IAllConnections, IConnection, EditorPane, Tabs }

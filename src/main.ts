import { createApp } from 'vue'
import { createPinia } from 'pinia'
import App from './App.vue'
import 'virtual:windi.css'
import 'ant-design-vue/dist/antd.dark.css'

const pinia = createPinia()
createApp(App).use(pinia).mount('#app')

import path from 'path'
import vue from '@vitejs/plugin-vue'
import Components from 'unplugin-vue-components/vite'
import WindiCSS from 'vite-plugin-windicss'
import { defineConfig } from 'vite'
import { AntDesignVueResolver } from 'unplugin-vue-components/resolvers'

const resolve = (dir: string) => path.join(__dirname, dir)

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue(),
    WindiCSS(),
    Components({
      resolvers: [AntDesignVueResolver()],
    })],
  resolve: {
    alias: {
      '@': resolve('src'),
      'components': resolve('src/components'),
      'store': resolve('src/store'),
      'views': resolve('src/views'),
    },
  },
  optimizeDeps: {
    include: ['@ant-design/colors'],
  },
})
